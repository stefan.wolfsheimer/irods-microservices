# irods-microservices

Builds [iRODS](https://irods.org) microservices

> _The term "microservice" refers to a C procedure that performs a simple task as part of a distributed workflow system. Each microservice is small and well defined. Application programmers, systems administrators, and system programmers can use existing microservices, but can also write and compile new microservices into server code within the integrated Rule-Oriented Data System (iRODS). This system is a community-driven, open source, middleware data grid that enables researchers, archivists and other managers to share, organize, preserve, and protect sets of digital files._ ([irods4-microservices-book](https://irods.org/uploads/2015/01/irods4-microservices-book-web.pdf) p. 8)

Offers a (automated) way to manage building, packaging and publishing C++ microservices. Using this admittedly opinionated method should (hopefully) be usable for other organizations.

Might require replacing or changing your CMake file, see below.

The included Docker image is quite generic and also usable for locally developing and 
testing irods software.

This repository is meant to be cloned and used with your own GitLab. The CI part works on both self-managed and GitLab SaaS.

## CMake

A "Template" CMake project is included (CMakeLists.txt etc)

See example from this repo: [irods-example-microservices](https://gitlab.com/mkorthof/irods-example-microservices)

Features:

- uses clang and libs provided by irods-externals
- uses c++ stdlib from clang
- includes libarchive, jansson and curl packages
- runpaths in resulting shared libraries are set correctly
- tested on latest centos 7 and irods versions 4.2.8, 4.2.11

To build from GitLab CI, create a git repository, edit it's [gitlab-ci.yml](https://gitlab.com/mkorthof/irods-example-microservices/-/blob/main/.gitlab-ci.yml) and change `trigger:project` inside `build_job`. See details below.

Or alternatively, set it's url as `SRC_REPO` in main repo (for example if the source is on GitHub).

## Docker

Ready to use images to build microserices are available for download from GitLab [Container Registry](../../../container_registry), see left menu. Use `docker-build.sh` and `docker-run.sh` to create and run the irods container locally. It includes a full irods install and - besides for building - can be used to developing or (functional) testing.

### Image size

The default image 'centos7_irods' is about 2.5GB total size.

The Dockerfile has a few options to trim down the image size. For example the most space is saved by installing only **one** version of each of the 'irods externals' (build dependencies), instead of **all** versions. If `INSTALL_ALL_IRODS_EXTERNALS=0` is set, an image tagged "-slim" will be build which is about 875MB total size.

### iRODS

Initial installation of irods is done as root, after that both irods and postgres processes run under their service user. The default entrypoint installs and/or starts irods and db. Use `docker exec` to run iCommands or a shell.

The current local directory is bind mounted as '/builds' inside the container.

#### Users

All users have a working irods_environment.

| role  | username | password        | unix account |                 |
|-------|----------|-----------------|--------------|-----------------|
| admin | rods     | _IRODS_PASSWORD | irods        | service account |
| user  | user1    | _USER1_PASSWORD | user1        | "test" user     |
| user  | user2    | _USER2_PASSWORD | user2        | "test" user     |

### Scripts

Use these scripts to locally build, run and execute commands with docker

``` shell
./docker-build.sh

./docker-run.sh 

./docker-exec -u user1 centos7_irods:latest <command>

# build slim image:
INSTALL_ALL_IRODS_EXTERNALS=0 ./docker-build.sh
# or build for specific irods version:
IRODS_VERSION=4.2.11 ./docker-build.sh

# simulate ci build:
STAGE=build ./docker-run.sh
```

Also see `docker-run.sh -h`

### Docker Variables

- `IRODS_VERSION`
- `INSTALL_ALL_IRODS_EXTERNALS`

\+ see ARGS in head of [Dockerfile](Dockerfile)

## GitLab CI

Pipeline which automates the above, tests and publishes package:

- *image:* creates docker image with irods and dependencies which is used in stages below
- *build:* run `ci-build.sh` to build msi from `SRC_REPO` and create package
- *test:* run `ci-test.sh` installs and tests package using irule(s)
- *publish:* run `gitlab-api-pkg.sh` to publish package in gitlab's package registry

It triggers on pushing commits to main branch, or manually.

### Configuration

Edit `gitlab-ci.yml` and set at least `SRC_REPO`.

#### Runners

The 'slim' image should be small enough to build on GitLab's shared runners using their SaaS free tier.

With a self-managed Runner you should be able to build both the full and slim image without any issues. It should be configured to build Docker images using the Docker executer (DinD). If runner uses 'Docker socket binding', set `DOCKER_SOCKET` to `"true"`. More details: [docs.gitlab.com](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html).

Default tags are: `shared` and `docker` (as used by GitLab.com's shared runners).

#### Tests

In test stage `ci-test.sh` will first run pre scripts, then irods rules and lastly post scripts:

- executable shell scripts in: 'test/pre-scripts.d' (`chmod 555`)
- run irule -F '*.r' files in: 'test/irods-rules.d' and 'tests' dir from src repo
- post-scripts.d, e.g. for checks or cleanup (`chmod 555`)

### Variables

- `DOCKER_IMAGE`
- `DOCKER_SOCKET`
- `SRC_REPO`

\+ same vars as docker, see [above](#docker-variables)

## Usage

A multi-project pipeline should be triggered on commit. So, this repo's pipeline will run on every change you push to the CMake project repo (for details see [docs.gitlab.com](https://docs.gitlab.com/ee/ci/pipelines/multi_project_pipelines.html)).

After the pipeline finishes, the end result should be an irods image in the GitLab Container Registry and a tested rpm package artifact added to the Packages Registry.

||
|-:|
|![Image of multi-project pipeline](docs/pipeline.png "Multi-project pipeline")||
|_Pipeline triggered by 'irods-example-microservices'_ |

## Troubleshooting

- If the pipeline doesn't start check if `SRC_REPO` is set or else `workflow:rules` will prevent triggering.
- If the pipeline doesn't run from cmake project repo, verify that the correct gitlab project is set as `trigger` in `gitlab-ci.yml` in that repo.
- A Runner should be is available that supports building docker images and running the 'docker' command.
- Make sure irods version is set correctly in `DOCKER_IMAGE`, as it decides which package versions get installed from packages.irods.org repo during build.
- Irods version should match whats in defined in CMakeLists.txt e.g. `find_package(IRODS 4.2.11 EXACT REQUIRED)`
- If one or more jobs fail, try setting `CI_DEBUG_TRACE: "false"` and review job output logs.

## Examples

Besides [irods-example-microservices](https://gitlab.com/mkorthof/irods-example-microservices), you can try building these already exisiting public repositories.

Change `SRC_REPO` variable in main `gitlab-ci.yml` to one of these url's:

- https://github.com/chStaiger/wur-microservices
- https://github.com/UtrechtUniversity/irods-uu-microservices
- https://github.com/MaastrichtUniversity/irods-microservices (needs 4.2.6)

## Registry

Docker Images are available for download from GitLab [Container Registry](../../../container_registry)

MSI Packages created from the example repo's are available for download from GitLab [Package Registry](../../packages)

Or, use the menu on the left.

## Acknowledgements

- [SURF B.V.](https://www.surf.nl)
- [Christine Staiger](https://github.com/chStaiger) (wur-microservices)
- [Lazlo Westerhof](https://github.com/lwesterhof) (original [CMakeLists.txt](https://github.com/UtrechtUniversity/irods-uu-microservices/blob/main/CMakeLists.txt))
- [Mher Kazandjian](https://github.com/mherkazandjian) (improved docker files)
- [Stefan Wolfsheimer](https://github.com/stefan-wolfsheimer) (original docker files, code in 'app' dir)
