#!/bin/sh

# IRODS_VERSION="$( python -c "import json;print(str(json.load(open('/var/lib/irods/VERSION.json')).get('irods_version', '')))" )"

python /var/lib/irods/scripts/setup_irods.py --verbose --json_configuration_file=setup_irods_json/setup_irods_${IRODS_VERSION:-4.2.11}.json
# && { pgrep -a -n irodsServer || python /var/lib/irods/scripts/irods_control.py start; }
