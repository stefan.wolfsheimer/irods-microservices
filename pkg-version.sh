#!/bin/bash

# Print package name with irods and msi version
#
# USAGE:
# ./version <RPM> [PACKAGE_NAME] [IRODS_VERSION] [RELEASE_VERSION]
# ./version.sh irods-wur-microservices-4.2.8_0.9.0-1.rpm irods-wur-microservices
#
# OUTPUT:
#   <PACKAGE_NAME>-<IRODS_VERSION>/<RELEASE_VERSION>
#   irods-wur-microservices-4.2.11/1.0.0-1

if [ -n "$1" ] && [ -z "$RPM" ]; then
  RPM="$1"
fi

# If set, get package and version from args
if [ -n "$1" ] && [ -z "$PACKAGE_NAME" ]; then
  PACKAGE_NAME="$2"
fi
if [ -n "$3" ] && [ -z "$IRODS_VERSION" ]; then
  IRODS_VERSION="$3"
fi
if [ -n "$4" ] && [ -z "$RELEASE_VERSION" ]; then
  RELEASE_VERSION="$4"
fi

# First use filename to get package name
if [ -z "$PACKAGE_NAME" ]; then
  PACKAGE_NAME="$(echo "$RPM" | sed -n -r "s|^(.*)-[0-9.-]+_.*\.(deb\|rpm)$|\1|p" 2>/dev/null)"
fi

# If version vars are still empty, try cmake
if [ -z "$IRODS_VERSION" ] && [ -z "$RELEASE_VERSION" ] && [ -n "$PACKAGE_NAME" ]; then
  for i in CMakeLists.txt ./*/CMakeLists.txt; do
    if [ -s "$i" ] && grep -q "$PACKAGE_NAME" "$i"; then
      IRODS_VERSION="$(sed -n -r 's/.*IRODS ([0-9.]+) .*REQUIRED.*/\1/p' "$i" 2>/dev/null)"
      RELEASE_VERSION="$(sed -n -r 's/.*RELEASE_VERSION "([0-9.]+)".*/\1/p' "$i" 2>/dev/null)"
    fi
  done
fi

# Try filename
if [ -z "$IRODS_VERSION" ]; then
  if [ -n "$PACKAGE_NAME" ]; then
    IRODS_VERSION="$(echo "$RPM" | sed -n -r "s|^${PACKAGE_NAME}-([0-9.-]+).*\.(deb\|rpm)$|\1|p" 2>/dev/null)"
  else
    IRODS_VERSION="$(echo "$RPM" | sed -n -r "s|^.*-([0-9.-]+)_.*\.(deb\|rpm)$|\1|p" 2>/dev/null)"
  fi
fi
if [ -z "$RELEASE_VERSION" ]; then
  RELEASE_VERSION="$(echo "$RPM" | sed -n -r "s|^.*_([0-9.-]+).(deb\|rpm)$|\1|p" 2>/dev/null)"
fi
if [ -z "$PACKAGE_NAME" ] && [ -z "$IRODS_VERSION" ] && [ "$RELEASE_VERSION" ]; then
  if [ -n "$RPM" ]; then
    IFS=" " read -r PACKAGE_NAME IRODS_VERSION RELEASE_VERSION <<<"$(echo "$RPM" | sed -n -r "s|^(.*)-([0-9.-]+)_(.*)\.(deb\|rpm)$|\1 \2 \3|p" 2>/dev/null)"
  fi
fi

# Use installed irods version if needed
if [ -s "/var/lib/irods/VERSION.json" ]; then
  IRODS_VERSION="$(sed -n -r 's/".*irods_version": "([0-9.]+)".*/\1/p' /var/lib/irods/VERSION.json 2>/dev/null)"
fi

# Lastly, use commit from GitLab CI or Git instead
if [ -z "$RELEASE_VERSION" ]; then
  if [ -n "$CI" ]; then
    RELEASE_VERSION="$CI_COMMIT_SHORT_SHA"
  else
    RELEASE_VERSION="$(git rev-parse --short HEAD)"
  fi
fi

echo "${PACKAGE_NAME:-irods-example-microservices}-${IRODS_VERSION:-0.0.0}/${RELEASE_VERSION:-0}"
