################################################################################
# Create iRODS image (CentOS) to build, dev and test microservices
################################################################################
FROM centos:7
ARG IRODS_VERSION=*
ARG INSTALL_ALL_IRODS_EXTERNALS=1
ARG INSTALL_ALL_IRODS_PLUGINS=0
ARG INSTALL_DEVTOOLS=0
ARG INSTALL_PYTHON=0
ARG INSTALL_OPENSTACK_CLIENTS=0
ARG INSTALL_CATCH=0

################################################################################
# Setup base, pkgs
################################################################################
# Install gosu
RUN curl -sSL https://github.com/tianon/gosu/releases/download/1.14/gosu-amd64 -o /usr/local/bin/gosu && \
    curl -sSL -O https://github.com/tianon/gosu/releases/download/1.14/gosu-amd64.asc && \
    gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && \
    gpg --batch --verify gosu-amd64.asc /usr/local/bin/gosu && \
    chmod +x /usr/local/bin/gosu && \
	  gosu nobody true && \
    test -n "$http_proxy" && echo "proxy=${http_proxy}" >>/etc/yum.conf; \
# Skip a few slow yum mirrors
    test "$CI" && echo "exclude=tripadvisor.com, cmich.edu, nodesdirect.com, velocihost.net" >> \
      /etc/yum/pluginconf.d/fastestmirror.conf; \
# Update, install epel and packages
    yum update -y && \
    yum install -y epel-release && \
# Install PostgreSQL
    yum install -y \
      authd \
      postgresql \
      postgresql-odbc\
      postgresql-server \
# Include required libs and tools
      libstdc++-static \
      libcurl-devel \
      ca-certificates \
      chrpath \
      coreutils \
      gcc-c++ \
      git \
      gnupg \
      make \
      libtool \
      less \
      lsof \
      openssl \
      openssl-devel \
      python3 \
      python3-pip \
      rpm-build \
      rpm-devel \
      rpmlint \
      rpmdevtools \
      rpmrebuild \
      rsync \
      s3cmd \
      unixODBC \
      wget \
      which \
# Include extra libs
      libarchive-devel \
      jansson-devel \
      librabbitmq-devel \
      libuuid-devel && \
# [optional] Install dev tools
    if [ ${INSTALL_DEVTOOLS:-0} -eq 1 ]; then \
      yum install -y \
        autoconf \
        automake \
        boost-devel \
        cmake \
        diffutils \
        patch \
        fuse-libs \
        gcc \
        help2man \
        perl-JSON \
        pam-devel \
        unixODBC-devel; \
      fi && \
    yum clean all

################################################################################
# Python, OpenStack clients, Catch22 and rpmbuild
################################################################################
# Always upgrade pip3 and install iRODS Client
RUN python3 -m pip install --no-cache-dir --upgrade pip && \
    python3 -m pip install --no-cache-dir python-irodsclient && \
# [optional] Install extra modules
    if [ ${INSTALL_PYTHON:-0} -eq 1 ]; then \
      yum update -y && \
      yum install -y \
        python2-behave \
        python2-devel \
        python2-distro \
        python2-jsonschema \
        python2-psutil \
        python2-pip \
        python2-requests \
        python3-distro \
        python3-devel \
        python3-requests && \
      yum clean all && \
        python2 -m pip install --no-cache-dir \
        python-irodsclient \
        jsonschema \
        flask==1.1.4 \
        flask_restful==0.3.9 \
        markupsafe==1.1.1 && \
      python3 -m pip install --no-cache-dir \
        breathe \
        jsonschema \
        flask \
        flask_restful \
        psutil \
        pytest \
        pyodbc \
        sphinx \
        setuptools_rust \
        xmlrunner; \
    fi

# [optional] OpenStack clients
RUN if [ ${INSTALL_OPENSTACK_CLIENTS:-0} -eq 1 ]; then \
  python -m pip install --no-cache-dir \
  python-swiftclient \
  python-keystoneclient \
  python-openstackclient; \
fi

# [optional] Install Catch2 (unit test c++)
RUN if [ ${INSTALL_CATCH:-0} -eq 1 ]; then \
      cd /opt && \
      curl -sSL -O https://github.com/catchorg/Catch2/archive/v2.9.2.tar.gz && \
      tar -xvf v2.9.2.tar.gz && \
      mkdir /opt/include && \
      ln -s /opt/Catch2-2.9.2/include/ /opt/include/catch2 && \
      rm -fr Catch2-2.9.2; \
    fi

# Setup rpmbuild
RUN useradd -r -m -d /home/rpmbuild -s /bin/bash rpmbuild
COPY --chown=rpmbuild:rpmbuild app/build_rpm.sh /home/rpmbuild/build_rpm.sh
RUN rpmdev-setuptree
USER rpmbuild 
RUN chown -R rpmbuild: /home/rpmbuild

################################################################################
# Install iRODS
################################################################################
# Repos and packages
USER root
RUN yum update -y && \
    rpm --import https://packages.irods.org/irods-signing-key.asc && \
    curl -s https://packages.irods.org/renci-irods.yum.repo | \
      tee /etc/yum.repos.d/renci-irods.yum.repo && \
    rpm --import https://core-dev.irods.org/irods-core-dev-signing-key.asc && \
    curl -s https://core-dev.irods.org/renci-irods-core-dev.yum.repo | \
      tee /etc/yum.repos.d/renci-irods-core-dev.yum.repo && \
    yum install -y \
      irods-devel-${IRODS_VERSION:-*} \
      irods-server-${IRODS_VERSION:-*} \
      irods-database-plugin-postgres-${IRODS_VERSION:-*} \
      irods-icommands-${IRODS_VERSION:-*} \
      irods-runtime-${IRODS_VERSION:-*} \
# Always install Python Rule Engine
      irods-rule-engine-plugin-python-${IRODS_VERSION:-*}; \
# [optional] Install All iRODS Rule Engine Plugins
      if [ ${INSTALL_IRODS_PLUGINS:-0} -eq 1 ]; then \
        yum install -y irods-rule-engine-plugin-*${IRODS_VERSION:-*}; \
      fi && \
    yum clean all

COPY app/odbcinst.ini /etc/odbcinst.ini

# Scripts
COPY app/irods_environment.json /root/.irods/irods_environment.json
COPY app/sleep.sh /app/sleep.sh
COPY app/wait_for_pg.sh /app/wait_for_pg.sh
COPY app/init_users.sh /app/init_users.sh
COPY app/wait_for_irods.sh /app/wait_for_irods.sh

# Create user1 and user2
RUN useradd -r -m -d /home/user1 -s /bin/bash user1 && \
    useradd -r -m -d /home/user2 -s /bin/bash user2
COPY --chown=user1:user1 app/irods_environment_user1_localhost.json /home/user1/.irods/irods_environment.json
COPY --chown=user2:user2 app/irods_environment_user2_localhost.json /home/user2/.irods/irods_environment.json

# Setup preparation
COPY 4_2_x/app/setup_answers.txt /app/setup_answers.txt
COPY 4_2_x/app/setup_irods.sh /app/setup_irods.sh
COPY 4_2_x/app/restart_irods.sh /app/restart_irods.sh
RUN groupadd -r irods && \
    useradd -r -d /var/lib/irods -M -s /bin/bash -g irods -c 'iRODS Administrator' -p '!' irods
COPY 4_2_x/config.mk /opt/config.mk

# [optional] iRODS externals: all = 3GB, minimal = 2.1GB  
RUN if [ ${INSTALL_ALL_IRODS_EXTERNALS:-1} -eq 1 ]; then \
      yum install -y 'irods-externals*'; \
    else \
# Install latest irods-externals versions only
      yum list -q available >/tmp/yum.list && \
      for i in autoconf avro1.7 aws-sdk-cpp1.4 boost1.60 catch22.13 clang-runtime6 clang6 cmake3.11 cppzmq4.2 cpr1 elasticlient0 epm \
               fmt6 imagemagic jansson json3 libarchive3.1 libs mungefs1.0.3 nanodbc2 pistache qpid-with-proton0.34 redis4 spdlog0 zeromq; do \
          OUT="$OUT $(awk '{if ($0 ~ /^irods-externals-'$i'/) a=$1} END{print a}' /tmp/yum.list)"; \
      done; \
      test -n "$OUT" && yum install -y $OUT; \
    fi; \
    yum clean all; \
    rm -rf /tmp/yum.list

################################################################################
# Configure PostgreSQL 9
################################################################################
COPY --chown=postgres:postgres app/run_postgres.sh /app/run_postgres.sh
USER postgres
RUN /usr/bin/initdb --pgdata="/var/lib/pgsql/data" --auth=ident && \
    for i in "127.0.0.1/32" "172.16.0.0/12" "fe80::/10"; do \
      echo "host all  all    $i md5" >> /var/lib/pgsql/data/pg_hba.conf; \
    done && \
    echo "listen_addresses='*'" >> /var/lib/pgsql/data/postgresql.conf && \
    /usr/bin/pg_ctl start -D /var/lib/pgsql/data -o "-p 5432" -w -t 300 && \
    /usr/bin/psql --command "CREATE USER docker WITH SUPERUSER PASSWORD 'docker';" && \
    /usr/bin/createdb -O docker docker && \
    /usr/bin/psql --command "CREATE USER irods WITH PASSWORD 'irods';" && \
    /usr/bin/psql --command 'CREATE DATABASE "ICAT";' && \
    /usr/bin/psql --command 'GRANT ALL PRIVILEGES ON DATABASE "ICAT" TO irods;' && \
    /usr/bin/pg_ctl stop -D /var/lib/pgsql/data -m fast

USER root
ENTRYPOINT [ "/app/setup_irods.sh" ]
CMD [ "/app/sleep.sh" ]
