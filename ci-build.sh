#!/bin/sh

# Build software using irods-externals (microservices)

if [ "$CI_DEBUG_TRACE" = "true" ]; then
  set -x
fi
make clean >/dev/null 2>&1 || true
rm -rf -- cmake _CPack_Packages CMakeFiles CMakeCache.txt install_manifest.txt Makefile ./*.cmake ./libmsi*.so ./*.rpm || true
PATH="$(find /opt/irods-externals/cmake*/bin -maxdepth 0 -printf '%p\n' | sort -r --version-sort | head -1)":$PATH \
  cmake . &&
  make &&
  QA_RPATHS=0x0002 make package
set +x
