#!/bin/bash
#
# Run local container and/or simulate GitLab CI stages e.g.
#   STAGE=build ./docker-run.sh
#
# ( first run ./docker-buid.sh to create image )
#

# Example repositories
if [ -z "${SRC_REPO}" ]; then
  SRC_REPO="https://gitlab.com/mkorthof/irods-example-microservices.git"
  #src_repo="https://git.wageningenur.nl/rdm-infrastructure/irods-microservices"
  #SRC_REPO="https://github.com/chStaiger/wur-microservices"
  #SRC_REPO="https://github.com/UtrechtUniversity/irods-uu-microservices"
  #SRC_REPO="https://github.com/MaastrichtUniversity/irods-microservices" # 4.2.6
fi

# Docker run variables
NAME="irods"
CT_HOSTNAME="icat.irods"

# GitLab variables
#CI_PROJECT_ID="123"
#TOKEN="abc"   # Use PAT instead of $CI_TOKEN (scope: api, write_repository)

if echo "$@" | grep -Eq -- "(-h| help)"; then
  cat <<-_EOF_

  Run local container and/or simulate GitLab CI stages:
    STAGE         "build|test|publish|iteractive"

  Other environment variables:
    SRC_REPO      "https://github.com/myname/my-microservices"
    DOCKER_IMAGE  "centos7_irods:4.2.11"

  Example:
    STAGE=build ./docker-run.sh"

  Without STAGE env var set, script starts irods in background

_EOF_
  exit 0
fi

#set -x

if [ "${STAGE}" = "build" ] && [ -z "${SRC_REPO}" ]; then
  echo "ERROR: missing 'SRC_REPO'"
  exit 1
fi

if [ -z "${DOCKER_IMAGE}" ]; then
  DOCKER_IMAGE="centos7_irods:latest"
fi
if [ -z "${CT_HOSTNAME}" ]; then
  CT_HOSTNAME="icat.irods"
fi

# rpm-package
set_rpm_file() {
  i=0
  for f in ./*.rpm; do
    test -s "$f" && i=$((i + 1))
  done
  if [ "${i:-0}" -eq 1 ]; then
    RPM="$(basename "$f")"
  else
    echo "ERROR: $i rpm(s) found"
    exit 1
  fi
}

# repository-dir
set_repo_dir() {
  _tmp="${SRC_REPO##*\/}"
  REPO_DIR="${_tmp%%.git}"
}

# irods-version
set_irods_ver() {
  _tmp="${DOCKER_IMAGE##*:}"
  IRODS_VERSION="${_tmp%%-slim}"
}

# check docker logs to verify irods is started
wait_for_irods() {
  i=1
  m=60
  while ! docker logs "$1" 2>/dev/null | grep -qa '<MsgHeader_PI>'; do
    if [ "$i" -gt $m ]; then
      echo "ERROR: failed to start irods"
      break
    fi
    echo "Waiting for irods to start... check ${i}/${m}"
    sleep 3
    i=$((i+1))
  done
}

case $STAGE in
interactive)
  docker run "$@" -ti \
    --entrypoint '/bin/sh' \
    --volume="$PWD":/host \
    --rm \
    --workdir /host \
    "${DOCKER_IMAGE}"
  ;;

build)
  set_repo_dir
  # shellcheck disable=SC1004
  docker run "$@" \
    --env VERBOSE=1 \
    --entrypoint '/bin/sh' \
    --volume="$PWD":/builds \
    --rm \
    --workdir /builds \
    "${DOCKER_IMAGE}" \
    -c 'git clone '"${SRC_REPO}"'; \
        ./patch.sh '"${REPO_DIR}"' '"${CI_SERVER_URL}"'; \
        ( cd '"${REPO_DIR}"' || exit 1;
          ../ci-build.sh && mv ./*.rpm .. || exit 1 )'
  ;;
test)
  set_rpm_file
  set_repo_dir
  docker run "$@" \
    --detach \
    --hostname "${CT_HOSTNAME}" \
    --name "${NAME}-test" \
    --env REPO_DIR="${REPO_DIR}" \
    --env PG_HOST="${CT_HOSTNAME}" \
    --volume="$PWD":/builds \
    --rm \
    --workdir /builds \
    "${DOCKER_IMAGE}"
  #sleep 30
  wait_for_irods "${NAME}-test"
  docker exec "$@" --interactive "${NAME}-test" yum localinstall -y "$RPM"
  docker exec "$@" --user user1 --interactive "${NAME}-test" /builds/ci-test.sh
  docker rm -f "${NAME}-test" 2>/dev/null || true
  ;;
publish)
  set_irods_ver
  set_rpm_file
  PKG_VER="$(./pkg-version.sh "$RPM")"
  docker run "$@" \
    --entrypoint '/bin/bash' \
    --env TOKEN="${TOKEN}" \
    --env IRODS_VERSION="${IRODS_VERSION}" \
    --volume="$PWD":/builds \
    --workdir=/builds \
    --rm \
    "${DOCKER_IMAGE}" \
    ./gitlab-api-pkg.sh "$RPM" "$PKG_VER" |
    grep -q "201 Created" && echo "OK: $RPM" || exit 1
  ;;
*)
  if ! docker ps -a --format='{{.Image}} {{.Names}}' | grep -Eq "$NAME" || [ "${FORCE:-0}" -eq 1 ]; then
    if [ "${FORCE:-0}" -eq 1 ]; then
      docker rm -f -v "$NAME"
    fi
    docker run "$@" \
      --detach \
      --hostname "${CT_HOSTNAME}" \
      --name "${NAME}" \
      --volume="$PWD":/builds \
      "${DOCKER_IMAGE}"
  else
    echo "Container '$NAME' already exists. To remove it, run: 'FORCE=1 ./docker-run.sh'"
  fi
  ;;
esac
#set +x
