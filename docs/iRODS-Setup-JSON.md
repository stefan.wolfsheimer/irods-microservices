# Setup

Unattended/automated installation using `setup_irods.py` with json file providing configuration settings.

Can be used as an alternative method instead of providing setup answers using stdin redirection.

- https://funinit.wordpress.com/2019/05/04/ansible-based-automated-deployment-of-irods-grid/
- https://github.com/irods/irods/blob/main/packaging/server_config.json.template
- https://github.com/spachika/irods-automation/blob/master/unattended_installation.json

## Examples

See included dir [extras/setup_irods_json](extras/setup_irods_json) and [setup_irods_json.sh](extras/setup_irods_json.sh)

## Config

Note that keys/values may differ per irods version

Create setup_irods.json from existing setup with `iozonereport`:

- extract `"host_access_control_config"`, `"hosts_config"` and `"service_account_environment"`
- set irods keys `"negotiation_key"`, `"zone_key"`: replace "XXXX..."
- add: `"admin_password": "MyPass123"`

## Run

`python /var/lib/irods/scripts/setup_irods.py --verbose --json_configuration_file=setup_irods.json`

Make sure irods gets started, newer versions (e.g. 4.2.11) don't do this after setup finishes
