#!/bin/bash

# Build docker image locally
# e.g. DOCKER_IMAGE=centos7_irods:4.2.11 ./docker-build.sh

#set -x
if [ -z "${DOCKER_IMAGE}" ]; then
  DOCKER_IMAGE="centos7_irods:4.2.11"
fi

if [ -z "${IRODS_VERSION}" ]; then
  _tmp="${DOCKER_IMAGE##*:}"
  IRODS_VERSION="${_tmp%%-slim}"
fi

if [ "${INSTALL_ALL_IRODS_EXTERNALS:-1}" -eq 0 ]; then
  SUFFIX="-slim"
fi
BUILD_IMAGE="${DOCKER_IMAGE}${SUFFIX}"

#docker pull "${DOCKER_IMAGE}" || true
docker build $* \
  --cache-from "${DOCKER_IMAGE}" \
  --tag "${DOCKER_IMAGE}" \
  --build-arg IRODS_VERSION="${IRODS_VERSION}" \
  --build-arg INSTALL_ALL_IRODS_EXTERNALS="${INSTALL_ALL_IRODS_EXTERNALS:-1}" \
  --build-arg INSTALL_ALL_IRODS_PLUGINS="${INSTALL_ALL_IRODS_PLUGINS:-0}" \
  --build-arg INSTALL_DEVTOOLS="${INSTALL_DEVTOOLS:-0}" \
  --build-arg INSTALL_PYTHON="${INSTALL_PYTHON:-0}" \
  --build-arg INSTALL_OPENSTACK_CLIENTS="${INSTALL_OPENSTACK_CLIENTS:-0}" \
  --build-arg INSTALL_CATCH="${INSTALL_CATCH:-0}" \
  --build-arg http_proxy="${http_proxy:-$HTTP_PROXY}" \
  .
docker tag "${BUILD_IMAGE}" "${BUILD_IMAGE%%:*}:latest"
#set +x
