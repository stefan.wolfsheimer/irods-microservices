#!/bin/bash
mkdir -p /etc/irods/ssl
chown irods /etc/irods/ssl

gosu irods openssl genrsa -out /etc/irods/ssl/server.key
gosu irods chmod 600 /etc/irods/ssl/server.key
gosu irods openssl req -new -x509 -key /etc/irods/ssl/server.key -out /etc/irods/ssl/server.crt -days 10000 -subj "/C=AU/ST=Some-State/O=Internet Widgits Pty Ltd/CN=localhost"
gosu irods cat /etc/irods/ssl/server.crt > /etc/irods/ssl/chain.pem
gosu irods openssl dhparam -2 -out /etc/irods/ssl/dhparams.pem 2048

mkdir -p /etc/pki/tls/certs/irods/
/app/configure_ssl.py
cp /etc/irods/ssl/server.crt /etc/pki/tls/certs/irods/server.crt
gosu irods /var/lib/irods/irodsctl restart

