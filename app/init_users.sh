#!/bin/bash
set -e
#set -x
_USER1_PASSWORD=${USER1_PASSWORD:-user1}
_USER2_PASSWORD=${USER2_PASSWORD:-user2}

for user in user2 user1
do
    if [ $( gosu irods iadmin lu | grep -c '^'$user'#' ) == "0" ]
    then
        pvar="_"$( echo $user | sed 's/./\U&/g' )"_PASSWORD"
        gosu irods iadmin mkuser $user rodsuser
        gosu irods iadmin moduser $user password ${!pvar}
    fi
done

echo $_USER1_PASSWORD | gosu user1 iinit
echo $_USER2_PASSWORD | gosu user2 iinit
